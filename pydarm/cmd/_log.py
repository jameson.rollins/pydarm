import logging

logger = logging.getLogger('pydarm')


class CMDError(Exception):
    pass
