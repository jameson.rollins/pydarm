import numpy as np

import git

from gpstime import gpstime

from ._log import logger
from ._git import check_report_git_status
from ._const import IFO, CALCS_FILTER_FILE
from ._report import Report
from . import _args
from . import _util
from .report_utils import FREE_PARAM_LABEL_MAP, STAGE_LABEL_MAP
from ..utils import (write_filter_module, write_dict_epics,
                     read_chain_from_hdf5, list_from_foton_zp_format,
                     )
from ..model import Model
from ..actuation import ActuationModel
from ..sensing import SensingModel


def read_epics_records_file(report):
    """Load Epics Channel records to export."""
    dtype = np.dtype([('name', 'U64'), ('value', 'f')])
    er = np.loadtxt(report.gen_path("export_epics_records.txt"), dtype=dtype)
    return dict(er)


def add_args(parser):
    _args.add_config_option(parser)
    mgroup = parser.add_mutually_exclusive_group()
    _args.add_report_option(mgroup)
    _args.add_model_option(mgroup, default=None)
    parser.add_argument(
        '--push', action='store_true',
        help="push filters and TDCF EPICS records to front end"
    )
    parser.add_argument(
        '--epics-only', action='store_false', dest='export_filters',
        help="only process EPICS changes")
    parser.add_argument(
        '--filters-only', action='store_false', dest='export_epics',
        help="only process Foton Filter updates.")


def main(args):
    """export parameters to front-end CALCS model

    This takes the parameters from the latest (or specified) report
    and writes them to the CALCS front end, either as EPICS records or
    foton filters.  This should be done only if the report to be
    exported has been properly reviewed.

    """
    config = _util.load_config(args.config)
    model_file = _args.args_get_model(args)
    sensing_model = SensingModel(model_file)
    report = Report.find(args.report)
    report.load_measurements()

    # check that the report git repo is clean
    check_report_git_status(report, check_valid=True)

    report_int = report.id_int()
    hash_int = report.git_int()

    # process Foton filters
    if args.export_filters:
        logger.info(f"filter file: {CALCS_FILTER_FILE}")

        model = Model(model_file)
        model_dict = model.config_to_dict()

        # sensing params
        optical_gain = float(model_dict['sensing']['coupled_cavity_optical_gain'])
        freq_cc = float(model_dict['sensing']['coupled_cavity_pole_frequency'])

        # actuation params
        act_free_params = {}
        calcs_keys = config['Common']['calcs_filters'].keys()
        for k in calcs_keys:
            if '/' not in k:
                # skip over sensing function params
                continue

            stage, optic = k.split('/')
            arm_section = f'actuation_{optic[-1].lower()}_arm'

            act_model = ActuationModel(model_file, arm_section)

            stage_homonym = STAGE_LABEL_MAP[stage]['homonym']
            stage_unit_dpc = STAGE_LABEL_MAP[stage]['unit_drive_per_counts']
            stage_unit_nsc = STAGE_LABEL_MAP[stage]['unit_nospecial_char']

            # build structure to mimic get_act_mcmc_results() output
            act_free_params[k] = {
                'map': {
                    'H_A': getattr(act_model,
                                   f'{stage_homonym.lower()}_dc_gain_{stage_unit_dpc}')()
                },
                'Npct_scale': getattr(act_model,
                                      f'{stage_homonym}_{stage_unit_nsc}'.lower())
            }
            if stage == 'L2':
                act_free_params[k]['map']['H_A'] /= getattr(act_model, 'pum_coil_outf_signflip')

        # print free parameters
        print(f"Hc: {optical_gain:4.4e} :: ", end='')
        print(f"1/Hc: {1/optical_gain:4.4e}")
        print(f"Fcc: {freq_cc:4.4e} Hz")

        for k, v in act_free_params.items():
            stage, optic = k.split('/')
            textlabel = FREE_PARAM_LABEL_MAP[stage]['textlabel']
            map_vals = v['map']
            print(f"{textlabel}: {map_vals['H_A']:4.4e} N/A ", end='')
            print(f":: {map_vals['H_A']*v['Npct_scale']:4.4e} N/ct")

        if args.push:
            action = "writing  "
        else:
            action = ""
        logger.info(f"{action}filters (filter:bank | name:design string):")
        for param_name, fbanks in config['Common']['calcs_filters'].items():
            for fbank in fbanks:
                bank_name = fbank['bank_name']
                mod_name = fbank['mod_name']
                bank_slot = fbank['mod_slot']
                zeros = []
                poles = []
                if param_name == 'Hc':
                    # export inverse optical gain
                    gain_val = 1/optical_gain
                elif param_name == 'Fcc':
                    gain_val = 1
                    zeros = [freq_cc]
                    poles = [7000]
                elif param_name == 'SRCD2N':
                    chain = read_chain_from_hdf5(report.gen_path('sensing_mcmc_chain.hdf5'))
                    chain = chain[-1]
                    proc_sensing_meas = report.measurements['Sensing'][0]
                    z_str, p_str, _, g_str = \
                        proc_sensing_meas.inverse_sensing_srcd2n_foton_filter_zpk(chain,
                                                                                  cftd=True)
                    sign_char = '-' if str(sensing_model.sensing_sign).startswith('-') else ''
                    zeros = list_from_foton_zp_format(z_str)
                    poles = list_from_foton_zp_format(p_str)
                    gain_val = float(sign_char + g_str)

                elif param_name in act_free_params.keys():
                    # write actuation gain in Newtons/ct
                    gain_val = act_free_params[param_name]['map']['H_A'] \
                        * act_free_params[param_name]['Npct_scale']
                else:
                    continue

                bank_str = f"{bank_name}:{bank_slot}"
                design_str = f"{mod_name:>10}:zpk({zeros}, {poles}, {gain_val:4.4e})"
                logger.info(f"  {action}{bank_str:25}  {design_str}")
                if args.push:
                    from foton import FilterFile
                    foton_obj = FilterFile(CALCS_FILTER_FILE)
                    write_filter_module(
                        foton_obj, bank_name, bank_slot-1, mod_name,
                        (zeros, poles, gain_val),
                    )

    # process EPICS records
    if args.export_epics:
        logger.info("Processing EPICS records")
        epics_records = read_epics_records_file(report)
        write_dict_epics(epics_records, dry_run=(not args.push), IFO=IFO)

    # TODO: add ability to clear entries. this is done by setting Section.design = ''.

    # update the front end report status channels
    write_dict_epics(
        {
            'CAL-CALIB_REPORT_ID_INT': report_int,
            'CAL-CALIB_REPORT_HASH_INT': hash_int,
        },
        dry_run=(not args.push), IFO=IFO,
    )

    # git tag the report as exported
    repo = git.Repo(report.path)
    gt = gpstime.parse("now")
    ts = _util.gen_timestamp(gt)
    tag = f"exported-{ts}"
    logger.info(f"git tagging report as exported: {tag}")
    repo.create_tag(
        tag,
        message=f"This report was exported on {gt}.",
    )
