import os


# instrument designator
IFO = os.getenv('IFO')
SITE = None
if IFO:
    IFO = IFO.upper()
if not IFO:
    pass
elif IFO == 'H1':
    SITE = 'LHO'
elif IFO == 'L1':
    SITE = 'LLO'
else:
    raise ValueError(f"unknown IFO '{IFO}'")


CAL_ROOT = None
if IFO:
    CAL_ROOT = os.path.join(
        '/ligo/groups/cal/',
        IFO,
    )
CAL_ROOT = os.getenv(
    'CAL_ROOT',
    CAL_ROOT,
)
if not CAL_ROOT:
    raise ValueError("CAL_ROOT not specified.")


CAL_CONFIG_ROOT = os.getenv(
    'CAL_CONFIG_ROOT',
    os.path.join(CAL_ROOT, 'ifo'),
)
CAL_MEASUREMENT_ROOT = os.getenv(
    'CAL_MEASUREMENT_ROOT',
    os.path.join(CAL_ROOT, 'measurements'),
)
CAL_REPORT_ROOT = os.getenv(
    'CAL_REPORT_ROOT',
    os.path.join(CAL_ROOT, 'reports'),
)
CAL_UNCERTAINTY_ROOT = os.getenv(
    'CAL_UNCERTAINTY_ROOT',
    os.path.join(CAL_ROOT, 'uncertainty'),
)


CAL_TEMPLATE_ROOT = os.path.join(CAL_CONFIG_ROOT, 'templates')
DEFAULT_MODEL_PATH = None
DEFAULT_MODEL_FNAME = None
DEFAULT_CONFIG_PATH = None
DEFAULT_CONFIG_FNAME = None
DEFAULT_UNCERTAINTY_CONFIG_PATH = None
if IFO:
    DEFAULT_MODEL_FNAME = f'pydarm_{IFO}.ini'
    DEFAULT_CONFIG_FNAME = f'pydarm_cmd_{IFO}.yaml'

    DEFAULT_MODEL_PATH = os.path.join(
        CAL_CONFIG_ROOT,
        DEFAULT_MODEL_FNAME,
    )
    DEFAULT_CONFIG_PATH = os.path.join(
        CAL_CONFIG_ROOT,
        DEFAULT_CONFIG_FNAME,
    )
    DEFAULT_UNCERTAINTY_CONFIG_PATH = os.path.join(
        CAL_CONFIG_ROOT,
        f'pydarm_uncertainty_{IFO}.ini',
    )


CHANSDIR = None
if IFO:
    CHANSDIR = os.path.join(
        '/opt/rtcds',
        SITE.lower(),
        IFO.lower(),
        'chans'
    )
CHANSDIR = os.getenv(
    'CHANSDIR',
    CHANSDIR,
)

CALCS_FILTER_FILE = None
if CHANSDIR:
    CALCS_FILTER_FILE = os.path.join(
        CHANSDIR,
        f'{IFO}CALCS.txt',
    )


# default frequency response frequency array
DEFAULT_FREQSPEC = '0.01:5000:3000'


# maximum allowable time difference measurement and corresponding PCal
# measurement
PCAL_DELTAT_SECONDS_LIMIT = 20*60
