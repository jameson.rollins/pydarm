import os
import argparse
import subprocess
from copy import deepcopy

import numpy as np
from scipy.signal import freqresp
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import MultipleLocator
from matplotlib import ticker
from glob import glob
from subprocess import run

from .. import __version__
from ._log import logger, CMDError
from . import _args
from ._const import IFO, CAL_CONFIG_ROOT
from ._report import Report, list_all_reports, get_last_report_by_tag
from .measurement_set import MeasurementSet
from . import report_utils as ru
from ..utils import read_chain_from_hdf5, freqresp_to_mag_db_phase_str
from ..sensing import SensingModel
from ..plot import BodePlot
from ..fir import FIRFilterFileGeneration
from ..measurement import RoamingLineMeasurementFile
from .report_utils import print_mcmc_params, FREE_PARAM_LABEL_MAP
from .report_utils import write_foton_tf_to_file


def write_dtt_deltal_external_calib(report):
    """export deltal_external calibration for dtt template"""
    freqs = np.logspace(0, np.log10(7444), num=1000)
    calib = report.calcs.calcs_dtt_calibration(freqs).astype(np.complex128)
    outfile = report.gen_path("deltal_external_calib_dtt.txt")
    with open(outfile, 'w') as f:
        outstr = freqresp_to_mag_db_phase_str(freqs, calib, include_header=True)
        f.write(outstr)


def write_dtt_pcal_calib(report):
    """export pcal calibration for dtt template"""
    freqs = np.logspace(0, np.log10(7444), num=1000)
    calib = (report.pcal.compute_pcal_correction(freqs, endstation=True)
             ).astype(np.complex128)
    outfile = report.gen_path("pcal_calib_dtt.txt")
    with open(outfile, 'w') as f:
        outstr = freqresp_to_mag_db_phase_str(freqs, calib, include_header=True)
        f.write(outstr)


def write_epics_records_file(report):
    """export file containing epics records to be written on export."""
    er = report.calcs.compute_epics_records(gds_pcal_endstation=False, exact=True)
    outfile = report.gen_path("export_epics_records.txt")
    np.savetxt(outfile, list(er.items()), fmt='%s')


def gen_sensing_MCMC(report):
    """generate sensing MCMC

    """
    report = deepcopy(report)
    logger.info("running sensing MCMC...")

    config = report.config['Sensing']
    processed_sensing, meas_timestamp = report.measurements['Sensing']

    # FIXME: if a previous MCMC run exists for a measurement
    # that has matching run-time parameters (num steps, burn in,
    # and freq region) then use those results here instead of
    # re-running the MCMC here.
    priors_bound = np.array(config['params']['priors_bound'])
    if np.all(priors_bound == None):  # noqa: E711
        priors_bound = None

    processed_sensing.run_mcmc(
        fmin=config['params']['mcmc_fmin'],
        fmax=config['params']['mcmc_fmax'],
        priors_bound=priors_bound,
        burn_in_steps=config['params']['mcmc_burn_in'],
        steps=config['params']['mcmc_steps'],
        save_chain_to_file=report.gen_path(
            'sensing_mcmc_chain.hdf5'),
        save_map_to_file=report.gen_path(
            'sensing_mcmc_map.json'),
    )


def gen_actuation_MCMC(report):
    """generate actuation MCMC

    """
    report = deepcopy(report)
    for stage, optics in report.config['Actuation'].items():
        if stage == 'params':
            continue

        for optic, config in optics.items():
            if optic == 'params':
                continue

            name = f'Actuation/{stage}/{optic}'

            logger.info(f"running {name} MCMC...")

            processed_actuation, meas_timestamp = report.measurements[name]

            # FIXME: if a previous MCMC run exists for a measurement
            # that has matching run-time parameters (num steps, burn in,
            # and freq region) then use those results here instead of
            # re-running the MCMC here.
            priors_bound = np.array(config['params']['priors_bound'])
            if np.all(priors_bound == None):  # noqa: E711
                priors_bound = None

            processed_actuation.run_mcmc(
                fmin=config['params']['mcmc_fmin'],
                fmax=config['params']['mcmc_fmax'],
                priors_bound=priors_bound,
                burn_in_steps=config['params']['mcmc_burn_in'],
                steps=config['params']['mcmc_steps'],
                save_chain_to_file=report.gen_path(
                    f'actuation_{stage}_{optic}_mcmc_chain.hdf5'
                ),
                save_map_to_file=report.gen_path(
                    f'actuation_{stage}_{optic}_mcmc_map.json'
                ),
            )


def sensing_history(new_report, past_reports):
    """Generate sensing history plots."""
    # FIXME: write proper docstring

    new_report = deepcopy(new_report)
    past_reports = deepcopy(past_reports)

    config = new_report.config['Sensing']
    C_new = new_report.model.sensing

    logger.info(f"processing sensing history ending at {new_report.id}")
    # use the last exported report in the current epoch as reference.
    # if it isn't available then use the earliest valid report
    # FIXME: place reference finding logic below in a utility function
    if past_reports:
        ref_report = get_last_report_by_tag(past_reports, 'exported')
        ref_report_tag = 'last exported'
        if ref_report is None:
            ref_report = get_last_report_by_tag(past_reports[::-1], 'valid')
            ref_report_tag = 'first valid'
        if ref_report is None:
            ref_report = new_report
            ref_report_tag = 'this'
    else:
        ref_report = new_report
        ref_report_tag = 'this'
    logger.info(f"using {ref_report_tag} report as Sensing history reference: "
                f"{ref_report.id}")
    ref_tag = ref_report.id

    # Create reference sensing model
    C_ref = ref_report.model.sensing

    # calculate reference optical response
    C_ref_optical_response = SensingModel.optical_response(
        C_ref.coupled_cavity_pole_frequency,
        C_ref.detuned_spring_frequency,
        C_ref.detuned_spring_q,
        C_ref.is_pro_spring,
    )

    processed_sensing, timestamp = new_report.measurements['Sensing']

    # Get the common frequency axis and measurement info
    frequencies, measOpticalResponse, measOpticalResponseUnc = \
        processed_sensing.get_processed_measurement_response()

    angular_frequencies = 2*np.pi*frequencies

    refNormOpticalResponse = freqresp(
        C_ref_optical_response,
        angular_frequencies
    )[1]

    refOpticalResponse = refNormOpticalResponse * \
        C_ref.coupled_cavity_optical_gain

    # === Plotting setup
    xlim = [5, 1400]
    # Set up common title names
    tfp_title = "Optical response transfer functions\n(scaled by 1/$C_R$)"
    rp_title = "Optical response residuals\n(measurement/model)"
    subtitleText = f"All fixed parameters drawn from {new_report.model_file}"
    sp_titlesize = 12

    # sensing history figure
    sensing_hist_fig = plt.figure()
    sensing_hist_fig.suptitle(f"{IFO} sensing model history\n", fontsize=20)
    sensing_hist_fig.text(
        .5, .93,
        subtitleText,
        in_layout=True,
        horizontalalignment='center',
        transform=sensing_hist_fig.transFigure)

    # transfer function plot
    sensing_hist_tf_bode = BodePlot(fig=sensing_hist_fig, spspec=[221, 223])
    sensing_hist_tf_bode.ax_mag.set_title(tfp_title, fontsize=sp_titlesize)

    # residuals plot
    sensing_hist_res_bode = BodePlot(fig=sensing_hist_fig, spspec=[222, 224])
    sensing_hist_res_bode.ax_mag.set_title(rp_title, fontsize=sp_titlesize)

    figs = []
    sensing_hist_handles = []
    sensing_hist_labels = []

    # Compute the optical response based on MCMC parameters
    mcmc_params = new_report.get_sens_mcmc_results()
    mcmc_map = mcmc_params['map']

    mcmcNormOpticalResponse = freqresp(
        SensingModel.optical_response(
            mcmc_map['Fcc'],
            mcmc_map['Fs'],
            mcmc_map['Qs'],
            C_new.is_pro_spring),
        angular_frequencies)[1]

    mcmcOpticalResponse = mcmcNormOpticalResponse * \
        mcmc_map['Hc'] * \
        np.exp(
            -2*np.pi*1j*mcmc_map['tau_C'] *
            frequencies)

    # Select scaling for the history plot
    expscale = int(np.floor(np.log10(C_ref.coupled_cavity_optical_gain)))
    ax_mag = sensing_hist_tf_bode.ax_mag
    ax_mag.set_ylabel(f'Magnitude (ct/m) x $10^{{{expscale}}}$')
    scale = 10**expscale

    # Add reference model curve
    resp = mcmcOpticalResponse / scale
    sensing_hist_model_handle, _ = sensing_hist_tf_bode.plot(frequencies,
                                                             resp, zorder=1)
    sensing_hist_model_label = f"{ref_tag} model"
    sensing_hist_handles.append(sensing_hist_model_handle)
    sensing_hist_labels.append(sensing_hist_model_label)

    # add new_report measurement
    meas_optgain_err, _ = sensing_hist_tf_bode.error(frequencies,
                                                     measOpticalResponse/scale,
                                                     measOpticalResponseUnc,
                                                     fmt='.',
                                                     zorder=25)
    meas_optgain_label = f"{timestamp} measurement"
    sensing_hist_handles.append(meas_optgain_err)
    sensing_hist_labels.append(meas_optgain_label)

    # Add a null curve to keep the color-coding consistent on the
    # residuals plot
    sensing_hist_res_bode.plot([], [])
    vline, _ = sensing_hist_tf_bode.vlines(
        config['params']['mcmc_fmin'],
        color='black', lw=2)
    sensing_hist_tf_bode.vlines(config['params']['mcmc_fmax'], color='black',
                                lw=2)
    vline_label = (f"MCMC Fit Range: {config['params']['mcmc_fmin']} Hz"
                   f" to {config['params']['mcmc_fmax']} Hz")

    meas_model, fmin, fmax, measurement, chain = read_chain_from_hdf5(
        new_report.gen_path('sensing_mcmc_chain.hdf5')
    )

    # Set up the quantile levels
    quantileLevels = np.array([0.16, 0.5, 0.84])

    # Make corner plot
    # FIXME: this is a terrible workaround for our label mapping problem
    keylist = ['Hc', 'Fcc', 'Fs', 'Qs', 'tau_C']
    math_labels = [FREE_PARAM_LABEL_MAP[x]['mathlabel'] for x in keylist]
    corner_fig = ru.make_corner_plot(
        chain,
        mcmc_params,
        math_labels,
        quantileLevels,
        new_report.gen_path(
            "sensing_mcmc_corner.png"
        ),
        f"{timestamp} sensing function\nMCMC corner plot"
    )

    # We need an additional figure for the MCMC results comparison.
    # Setup is in the same format as the multi-measurement comparison.
    fig_mcmc = plt.figure()
    fig_mcmc.suptitle(f"{IFO} sensing model MCMC summary\n", fontsize=20)
    fig_mcmc.text(
        .5, .93,
        subtitleText,
        in_layout=True,
        horizontalalignment='center',
        transform=fig_mcmc.transFigure)

    tfp_mcmc = BodePlot(fig=fig_mcmc, spspec=[221, 223])
    tfp_mcmc.ax_mag.set_title(tfp_title, fontsize=sp_titlesize)
    tfp_mcmc.ax_mag.set_ylabel(f'Magnitude (ct/m) x $10^{{{expscale}}}$')
    rp_mcmc = BodePlot(fig=fig_mcmc, spspec=[222, 224])
    rp_mcmc.ax_mag.set_title(rp_title, fontsize=sp_titlesize)

    # Add the curves to the plot
    ref_resp_line, _ = tfp_mcmc.plot(
        frequencies,
        refOpticalResponse/scale)
    ref_resp_line_label = \
        f"Model w free params from report {ref_report.id}"

    mcmc_resp_line, _ = tfp_mcmc.plot(
        frequencies,
        mcmcOpticalResponse/scale)
    mcmc_resp_line_label = ("Model w free params from MCMC"
                            f"\nfit to {timestamp} data")

    meas_err_containers, _ = tfp_mcmc.error(
        frequencies,
        measOpticalResponse/scale,
        measOpticalResponseUnc,
        fmt='.',
        markersize=5)
    meas_err_containers_label = f"{timestamp} measurement"

    rp_mcmc.error(
        frequencies,
        measOpticalResponse/refOpticalResponse,
        measOpticalResponseUnc,
        fmt='.')
    rp_mcmc.error(
        frequencies,
        measOpticalResponse/mcmcOpticalResponse,
        measOpticalResponseUnc,
        fmt='.')
    rp_mcmc.ax_mag.set_yscale('linear')
    rp_mcmc.ax_mag.set_ylim(0.9, 1.1)
    rp_mcmc.ax_mag.set_xlim(*xlim)
    rp_mcmc.ax_phase.set_xlim(*xlim)
    rp_mcmc.ax_phase.set_ylim(-10, 10)
    ru.adjust_phase_yticks(rp_mcmc.ax_phase)
    rp_mcmc.ax_phase.yaxis.set_minor_locator(MultipleLocator(1))
    rp_mcmc.ax_mag.yaxis.set_minor_locator(MultipleLocator(.01))
    tfp_mcmc.ax_phase.set_ylim(-180, 180)
    tfp_mcmc.ax_phase.yaxis.set_minor_locator(MultipleLocator(30))
    tfp_mcmc.ax_mag.set_xlim(*xlim)
    tfp_mcmc.ax_phase.set_xlim(*xlim)

    # Add vertical lines marking the fit range for the MCMC
    vline_label = f"Fit range: [{fmin:0.2f}, {fmax:0.2f}] Hz"
    for p in [tfp_mcmc, rp_mcmc]:
        v_line = p.vlines(
            fmin, color='k', lw=2,
        )
        p.vlines(fmax, color='k', lw=2)

    fig_mcmc.legend(
        handles=[
            meas_err_containers[0],
            ref_resp_line,
            mcmc_resp_line,
            v_line[0]
            ],
        labels=[
            meas_err_containers_label,
            ref_resp_line_label,
            mcmc_resp_line_label,
            vline_label
            ],
        bbox_to_anchor=(.1, .82, .8, .1),
        loc='lower left',
        ncol=3,
        mode='expand',
        fontsize='small',
        markerscale=1,
        bbox_transform=fig_mcmc.transFigure
    )

    # setup params to print table
    table_params = {k: v for (k, v) in
                    FREE_PARAM_LABEL_MAP.items() if k in
                    ['Hc', 'Fcc', 'Fs', 'Qs', 'tau_C']
                    }
    _, mcmcTablePM = print_mcmc_params(chain,
                                       table_params, (.16, .5, .84))
    fig_mcmc.tight_layout(rect=(0, 0, 1, .91))
    tbox = fig_mcmc.text(
        .5, 0,
        "\n"*6+mcmcTablePM+"\n",
        fontfamily='monospace',
        size=10,
        horizontalalignment='center',
        verticalalignment='bottom',
        transform=fig_mcmc.transFigure,
    )

    text_bbox = tbox.get_tightbbox(
        renderer=fig_mcmc.canvas.get_renderer())
    text_height = text_bbox.y1-text_bbox.y0
    fig_height = fig_mcmc.get_size_inches()[1]*fig_mcmc.dpi
    adjust_fraction = (text_height)/fig_height
    fig_mcmc.subplots_adjust(bottom=adjust_fraction)
    fig_mcmc.set_size_inches(10, 10)
    fig_mcmc.savefig(
        new_report.gen_path(
            "sensing_mcmc_compare.png"
        ),
    )
    figs.append(fig_mcmc)
    figs.append(corner_fig)
    # Add meas curves to residuals plot
    sensing_hist_res_bode.error(
        frequencies,
        measOpticalResponse/mcmcOpticalResponse,
        measOpticalResponseUnc,
        fmt='.',
        zorder=25)
    sensing_hist_res_bode.ax_mag.set_yscale('linear')
    sensing_hist_res_bode.ax_mag.set_ylim(0.9, 1.1)
    sensing_hist_res_bode.ax_phase.set_ylim(-15, 15)
    ru.adjust_phase_yticks(sensing_hist_res_bode.ax_phase)

    for im, ireport in enumerate(past_reports):
        processed_sensing, timestamp = ireport.measurements['Sensing']

        # Get the common frequency axis and measurement info
        frequencies, measOpticalResponse, measOpticalResponseUnc = \
            processed_sensing.get_processed_measurement_response()

        angular_frequencies = 2*np.pi*frequencies

        mcmcNormOpticalResponse = freqresp(
            SensingModel.optical_response(
                mcmc_map['Fcc'],
                mcmc_map['Fs'],
                mcmc_map['Qs'],
                ireport.model.sensing.is_pro_spring),
            angular_frequencies)[1]

        mcmcOpticalResponse = mcmcNormOpticalResponse * \
            mcmc_map['Hc'] * \
            np.exp(
                -2*np.pi*1j*mcmc_map['tau_C'] *
                frequencies)

        # Add meas curves to transfer function comparison plots
        sensing_hist_err_container, _ = sensing_hist_tf_bode.error(
            frequencies,
            measOpticalResponse/scale,
            measOpticalResponseUnc,
            fmt='.',
            zorder=20-im)
        sensing_hist_tf_bode_label = f"{ireport.id} measurement"
        sensing_hist_handles.append(sensing_hist_err_container)
        sensing_hist_labels.append(sensing_hist_tf_bode_label)

        # Add meas curves to residuals plot
        sensing_hist_res_bode.error(
            frequencies,
            measOpticalResponse/mcmcOpticalResponse,
            measOpticalResponseUnc,
            fmt='.',
            zorder=20-im)
        sensing_hist_res_bode.ax_mag.set_yscale('linear')
        sensing_hist_res_bode.ax_mag.set_ylim(0.9, 1.1)
        sensing_hist_res_bode.ax_phase.set_ylim(-15, 15)
        ru.adjust_phase_yticks(sensing_hist_res_bode.ax_phase)

    sensing_hist_tf_bode.ax_phase.yaxis.set_major_locator(MultipleLocator(45))
    sensing_hist_tf_bode.ax_phase.set_ylim(-180, 180)
    sensing_hist_handles.append(vline)
    sensing_hist_labels.append(vline_label)
    sensing_hist_fig.tight_layout(rect=(0, 0, 1, .95))
    sensing_hist_fig.legend(
        handles=sensing_hist_handles,
        labels=sensing_hist_labels,
        bbox_to_anchor=(.05, .83, .9, .1),
        loc='lower left',
        ncol=3,
        mode='expand',
        fontsize='small',
        markerscale=1,
        bbox_transform=sensing_hist_fig.transFigure,
    )
    # nasty hack to avoid history fig when only one report in epoch
    if not new_report == ref_report:
        figs.append(sensing_hist_fig)

        # Wrap up and save figure
        sensing_hist_fig.savefig(
            new_report.gen_path(
                "sensing_tf_history.png"
            )
        )
    return figs


def actuation_history(new_report, past_reports):
    """generate actuation function MCMC and plots

    """
    # TODO: write proper docstring
    new_report = deepcopy(new_report)
    past_reports = deepcopy(past_reports)
    base_config = new_report.config['Actuation']

    # use the last exported report in the current epoch as reference.
    # if it isn't available then use the earliest valid report
    if past_reports:
        ref_report = get_last_report_by_tag(past_reports, 'exported')
        ref_report_tag = 'last exported'
        if ref_report is None:
            ref_report = get_last_report_by_tag(past_reports[::-1], 'valid')
            ref_report_tag = 'first valid'
        if ref_report is None:
            ref_report = new_report
            ref_report_tag = 'this'
    else:
        ref_report = new_report
        ref_report_tag = 'this'
    logger.info(f"Using {ref_report_tag} report as Actuation reference: "
                f"{ref_report.id}")

    epoch_act = ref_report.model.actuation
    ref_tag = ref_report.id

    # Set up common title names
    tfp_title = "Actuation strength transfer functions\n(scaled by $H_{ref}$)"
    rp_title = "Actuation strength residuals\n(meas./model w. free params)"
    subtitle_text = f"All fixed parameters drawn from {new_report.model_file}"
    sp_titlesize = 12

    figs = []

    # process by actuator stage
    mcmc_params = new_report.get_act_mcmc_results()
    for stage, optics in base_config.items():
        if stage == 'params':
            continue

        for optic, config in optics.items():
            if optic == 'params':
                continue

            # main figure
            act_hist_fig = plt.figure()
            act_hist_fig.suptitle(
                f"{IFO}SUS{optic}"
                f" {stage} actuation model history\n",
                fontsize=20)
            act_hist_fig.text(
                .5, .93,
                subtitle_text,
                horizontalalignment='center',
                transform=act_hist_fig.transFigure)

            # transfer function plot (comparison)
            tfp = BodePlot(fig=act_hist_fig, spspec=[221, 223])
            tfp.ax_mag.set_title(tfp_title, fontsize=sp_titlesize)

            # residuals plot (comparison)
            rp = BodePlot(fig=act_hist_fig, spspec=[222, 224])
            rp.ax_mag.set_title(rp_title, fontsize=sp_titlesize)

            # use actuation model from report at the last epoch boundary
            # as the reference
            epoch_act_arm = getattr(epoch_act, f'{optic[-1].lower()}arm')

            # gain_units, gain_to_NpCt_factor
            if stage == 'L1':
                actuator_strength_param = abs(epoch_act_arm.uim_npa)
                gain_units = 'N/A'
            elif stage == 'L2':
                actuator_strength_param = abs(epoch_act_arm.pum_npa)
                gain_units = 'N/A'
            elif stage == 'L3':
                actuator_strength_param = abs(epoch_act_arm.tst_npv2)
                gain_units = 'N/V**2'
            else:
                raise CMDError(f"unknown stage in config: {stage}")

            # process new_report measurement
            act_hist_handles = []
            act_hist_labels = []
            name = f'Actuation/{stage}/{optic}'
            processed_actuation_new, timestamp = new_report.measurements[name]

            # Get the common frequency axis and measurement info
            frequencies, meas_actuator_strength, meas_actuator_strength_unc = \
                processed_actuation_new.get_processed_measurement_response()

            # N/A or N/V**2, as taken from current ini
            actuator_strength = actuator_strength_param * np.ones(
                frequencies.shape)

            # Select scaling for the plot
            expscale = int(np.floor(np.log10(actuator_strength_param)))
            scale = 10**expscale
            label_text = f'Magnitude x $10^{{{expscale}}}$ ({gain_units})'
            tfp.ax_mag.set_ylabel(label_text)

            # Add reference model curve
            model_handle, _ = tfp.plot(frequencies,
                                       actuator_strength/scale,
                                       zorder=1)
            model_label = f"{ref_tag} model"
            act_hist_handles.append(model_handle)
            act_hist_labels.append(model_label)

            # Add a null curve to keep the color-coding consistent
            # to the residuals plot
            rp.plot([], [])

            meas_model, fmin, fmax, measurement, chain = read_chain_from_hdf5(
                new_report.gen_path(f'actuation_{stage}_{optic}_mcmc_chain.hdf5')
            )

            # Set up the quantile levels
            quantileLevels = np.array([0.16, 0.5, 0.84])

            # get mcmc params for this stage + optic configuration
            act_unit_params = mcmc_params['/'.join([stage, optic])]

            # Make corner plot
            # Determine which parameters labels are present and which math
            # labels
            # are associated
            # FIXME: this is a _terrible_ way to solve this label problem
            math_labels = [FREE_PARAM_LABEL_MAP[x]['mathlabel'] for x in
                           [stage, 'tau_A']]
            corner_fig = ru.make_corner_plot(
                chain,
                act_unit_params,
                math_labels,
                quantileLevels,
                new_report.gen_path(
                    f"actuation_{stage}_{optic}_mcmc_corner.png"
                ),
                f"{timestamp} {optic} {stage} actuation\nMCMC corner plot"
            )

            mcmc_actuator_strength = \
                act_unit_params['map']['H_A'] \
                * np.exp(-2*np.pi*1j
                         * act_unit_params['map']['tau_A']
                         * frequencies)

            fig_mcmc_handles = []
            fig_mcmc_labels = []
            fig_mcmc = plt.figure()
            fig_mcmc.suptitle(
                f"{IFO}SUS{optic} {stage} "
                "actuation model MCMC summary\n",
                fontsize=20)
            fig_mcmc.text(
                .5, .93,
                subtitle_text,
                in_layout=True,
                horizontalalignment='center',
                transform=fig_mcmc.transFigure)

            tfp_mcmc = BodePlot(fig=fig_mcmc, spspec=[221, 223])
            tfp_mcmc.ax_mag.set_title(tfp_title, fontsize=sp_titlesize)
            tfp_mcmc.ax_mag.set_ylabel(f'Magnitude ({gain_units})')
            rp_mcmc = BodePlot(fig=fig_mcmc, spspec=[222, 224])
            rp_mcmc.ax_mag.set_title(rp_title, fontsize=sp_titlesize)

            # Add the curves to the plot

            # plots actuation from ini file
            mcmc_model_free_line, _ = tfp_mcmc.plot(
                frequencies,
                actuator_strength)
            model_label = f"Model w free params from report {new_report.id}"
            fig_mcmc_handles.append(mcmc_model_free_line)
            fig_mcmc_labels.append(model_label)

            # plot actuation from mcmc fit
            mcmc_model_fit_line, _ = tfp_mcmc.plot(
                frequencies,
                mcmc_actuator_strength)
            mcmc_model_fit_label = ("Model w free params from"
                                    f"\nMCMC fit to {timestamp} data")
            fig_mcmc_handles.append(mcmc_model_fit_line)
            fig_mcmc_labels.append(mcmc_model_fit_label)

            mcmc_model_meas_handle, _ = tfp_mcmc.error(
                frequencies,
                meas_actuator_strength,
                meas_actuator_strength_unc,
                fmt='.',
                zorder=20)
            mcmc_model_meas_label = f"{timestamp} measurement"
            fig_mcmc_handles.append(mcmc_model_meas_handle)
            fig_mcmc_labels.append(mcmc_model_meas_label)

            rp_mcmc.error(
                frequencies,
                meas_actuator_strength/actuator_strength,
                meas_actuator_strength_unc,
                label=(
                    f"{timestamp} meas / model w free params\n"
                    f" from MCMC fit to {timestamp}"),
                fmt='.')
            rp_mcmc.error(
                frequencies,
                meas_actuator_strength/mcmc_actuator_strength,
                meas_actuator_strength_unc,
                label=(
                    f"{timestamp} meas / model w free params\n"
                    f" from report {new_report.id}"),
                fmt='.')
            rp_mcmc.ax_mag.set_yscale('linear')
            rp_mcmc.ax_mag.set_ylim(0.9, 1.1)
            rp_mcmc.ax_phase.set_ylim(-10, 10)
            ru.adjust_phase_yticks(rp_mcmc.ax_phase)

            # Add vertical lines marking the fit range for the MCMC
            for p in [tfp_mcmc, rp_mcmc]:
                mcmc_vline_handle, _ = p.vlines(
                    config['params']['mcmc_fmin'], color='k', lw=2)
                p.vlines(config['params']['mcmc_fmax'], color='k', lw=2)
            mcmc_vline_label = (f"Fit range {config['params']['mcmc_fmin']} to"
                                f" {config['params']['mcmc_fmax']} Hz")
            fig_mcmc_handles.append(mcmc_vline_handle)
            fig_mcmc_labels.append(mcmc_vline_label)

            fig_mcmc.tight_layout(rect=(0, 0, 1, .95))
            fig_mcmc.legend(
                handles=fig_mcmc_handles,
                labels=fig_mcmc_labels,
                bbox_to_anchor=(.05, .84, .9, .1),
                loc='lower left',
                ncol=3,
                mode='expand',
                fontsize='small',
                markerscale=1,
                bbox_transform=fig_mcmc.transFigure
            )

            table_params = {k: v for (k, v) in
                            FREE_PARAM_LABEL_MAP.items() if k in
                            [stage, 'tau_A']
                            }
            _, mcmcTablePM = print_mcmc_params(chain,
                                               table_params, (.16, .5, .84))
            tbox = fig_mcmc.text(
                .5, 0,
                "\n"*6+mcmcTablePM+"\n",
                fontfamily='monospace',
                size=10,
                horizontalalignment='center',
                verticalalignment='bottom',
                transform=fig_mcmc.transFigure,
            )

            text_bbox = tbox.get_tightbbox(
                renderer=fig_mcmc.canvas.get_renderer())
            text_height = text_bbox.y1-text_bbox.y0
            fig_height = fig_mcmc.get_size_inches()[1]*fig_mcmc.dpi
            adjust_fraction = (text_height)/fig_height
            fig_mcmc.subplots_adjust(bottom=adjust_fraction)
            fig_mcmc.savefig(
                new_report.gen_path(
                    f"actuation_{stage}_{optic}_mcmc_compare.png"),
            )
            figs.append(fig_mcmc)
            figs.append(corner_fig)

            # add new_report to history plot
            # Add meas curves to transfer function comparison plots
            act_hist_err_container, _ = tfp.error(
                frequencies,
                meas_actuator_strength/scale,
                meas_actuator_strength_unc,
                fmt='.',
                zorder=25)
            act_hist_err_label = f"{timestamp} measurement"
            act_hist_handles.append(act_hist_err_container)
            act_hist_labels.append(act_hist_err_label)

            # Add meas curves to residuals plot
            rp.error(
                frequencies,
                meas_actuator_strength/actuator_strength,
                meas_actuator_strength_unc,
                fmt='.',
                zorder=25)

            # ==== Loop over measurement dates/times
            for im, ireport in enumerate(past_reports):
                processed_actuation, timestamp = ireport.measurements[name]

                # Get the common frequency axis and measurement info
                frequencies, meas_actuator_strength, unc = \
                    processed_actuation.get_processed_measurement_response()
                meas_actuator_strength_unc = unc

                actuator_strength = actuator_strength_param * np.ones(
                    frequencies.shape)

                # Add meas curves to transfer function comparison plots
                act_hist_err_container, _ = tfp.error(
                    frequencies,
                    meas_actuator_strength/scale,
                    meas_actuator_strength_unc,
                    fmt='.',
                    zorder=20-im)
                act_hist_err_label = f"{timestamp} measurement"
                act_hist_handles.append(act_hist_err_container)
                act_hist_labels.append(act_hist_err_label)

                # Add meas curves to residuals plot
                rp.error(
                    frequencies,
                    meas_actuator_strength/actuator_strength,
                    meas_actuator_strength_unc,
                    fmt='.',
                    zorder=20-im)
            rp.ax_mag.set_yscale('linear')
            rp.ax_mag.set_ylim(0.9, 1.1)
            rp.ax_phase.set_ylim(-15, 15)
            ru.adjust_phase_yticks(rp.ax_phase)

            # nasty hack to avoid plotting issues when there is only
            # one report to process
            if not ref_report == new_report:
                figs.append(act_hist_fig)
            # add mcmc fit range to hist
            act_hist_vline, _ = tfp.vlines(config['params']['mcmc_fmin'], lw=2,
                                           color='k')
            tfp.vlines(config['params']['mcmc_fmax'], lw=2, color='k')
            rp.vlines(config['params']['mcmc_fmin'], lw=2, color='k')
            rp.vlines(config['params']['mcmc_fmax'], lw=2, color='k')
            act_hist_handles.append(act_hist_vline)
            act_hist_labels.append("MCMC Fit Range: "
                                   f"{config['params']['mcmc_fmin']:0.0f} Hz "
                                   f"to {config['params']['mcmc_fmax']:0.0f}"
                                   " Hz")

            tfp.ax_phase.yaxis.set_major_locator(MultipleLocator(45))
            act_hist_fig.tight_layout(rect=(0, 0, 1, .95))
            act_hist_fig.legend(
                handles=act_hist_handles,
                labels=act_hist_labels,
                bbox_to_anchor=(.05, .83, .9, .1),
                loc='lower left',
                ncol=3,
                mode='expand',
                fontsize='small',
                markerscale=1,
                bbox_transform=act_hist_fig.transFigure
            )

            # Wrap up and save figure
            act_hist_fig.savefig(
                new_report.gen_path(
                    f"actuation_{stage}_{optic}_tf_history.png"
                )
            )
    return figs


def sensing_GPR(reports):
    reports = deepcopy(reports)
    report = reports[0]

    # use the last exported report in the current epoch as reference.
    # if it isn't available then use the earliest valid report
    if len(reports) > 1:
        ref_report = get_last_report_by_tag(reports, 'exported')
        ref_report_tag = 'last exported'
        if ref_report is None:
            ref_report = get_last_report_by_tag(reports[::-1], 'valid')
            ref_report_tag = 'first valid'
        if ref_report is None:
            ref_report = report
            ref_report_tag = 'this'
    else:
        ref_report = report
        ref_report_tag = 'this'
    logger.info(f"Using {ref_report_tag} report as Sensing GPR reference: "
                f"{ref_report.id}")
    ref_idx = reports.index(ref_report)

    # try to get the gpr frequency range & plot range from
    # config file (cmd yaml). otherwise use the same defaults
    # we've used before.
    sensing_cfg = report.config['Sensing']['params']
    try:
        sensing_gpr_frange = [sensing_cfg['gpr_fmin'],
                              sensing_cfg['gpr_fmax']]
    except Exception:
        sensing_gpr_frange = [5, 4100]
        logger.warning("Unable to read parameter from config. "
                       f"Defaulting to gpr_freq_range={sensing_gpr_frange}")
    try:
        frange_plot = sensing_cfg['gpr_freq_range_plot']
    except Exception:
        frange_plot = [4, 4500]
        logger.warning("Unable to read parameter from config. "
                       f"Defaulting to frange_plot={frange_plot}")
    try:
        rbf_length_scale = sensing_cfg['rbf_length_scale']
    except Exception:
        rbf_length_scale = 1.0
        logger.warning("Unable to read parameter from config. "
                       f"Defaulting to rbf_length_scale={rbf_length_scale}")
    try:
        rbf_length_scale_limits = sensing_cfg['rbf_length_scale_limit']
    except Exception:
        rbf_length_scale_limits = [0.5, 1]
        logger.warning("Unable to read parameter from config. "
                       f"Defaulting to rbf_length_scale_limits={rbf_length_scale_limits}")

    # FIXME: take this from input args with default option
    frequencies = np.logspace(np.log10(1), np.log10(5000), 100)

    measurement_list = []
    timestamp_list = []
    fmin_list = []
    fmax_list = []
    for ireport in deepcopy(reports):
        processed_sensing, timestamp = ireport.measurements['Sensing']
        mcmc_data = ireport.get_sens_mcmc_results()

        # skip duplicate measurements
        if timestamp in timestamp_list:
            continue

        timestamp_list.append(timestamp)
        measurement_list.append(processed_sensing)
        fmin_list.append(mcmc_data['fmin'])
        fmax_list.append(mcmc_data['fmax'])

    # Now we will read the HF roaming line results and process them to be
    # included in the GPR
    hf_meas_filenames = glob(report.gen_mpath('hf_roaming_lines', 'DARM*.txt'))
    hf_pcal_unc_corr = os.path.join(CAL_CONFIG_ROOT,
                                    "pcal_high_frequency_uncertainty_O4a",
                                    "etm_deformation_uncertainty.txt")
    # FIXME: this handling of the HF measurement files is terrible.
    if hf_meas_filenames == []:
        logger.warning("No HFRL files found.")
    else:
        logger.info(f"Found HF tf files: {hf_meas_filenames}")
    hf_meas_files = [RoamingLineMeasurementFile(f, hf_pcal_unc_corr)
                     for f in hf_meas_filenames]

    # filter measurements by date; include only those valid during the
    # valid epoch, if any.
    # also ignore individual measurements where the coherence is nan

    # for determining which HF measurements to process use the
    # true epoch report as the starting bound
    last_valid_report = get_last_report_by_tag(reports[::-1], 'valid')
    if last_valid_report is not None:
        epoch_start = last_valid_report.id_gpstime()
    else:
        epoch_start = report.id_gpstime()
    epoch_end = report.id_gpstime()
    logger.info("HF epoch: "
                f"{last_valid_report.id if last_valid_report else report.id} "
                f"--- {report.id}")
    hf_measurements = []
    for hfm in hf_meas_files:
        # filter out any measurements is a coherence of NAN
        # mlist = [m for m in hfm.measurements if not np.isnan(m.coh)]
        mlist = []
        for m in hfm.measurements:
            if np.isnan(m.coh):
                logger.debug(f"Discarding NAN coherence HF measurements: {m}")
                continue
            mag_exp_scale = np.log10(np.abs(m.get_raw_tf()[1][0]))
            if mag_exp_scale > 10.:
                logger.debug("Discarding possible glitch of order "
                             f"10^{int(mag_exp_scale)}: {m}")
                continue
            # FIXME: indefensible hack to get rid of buggy calc with supposedly
            # high coherence.
            # need to fix the HFRL measurement code. -LD
            if m.navg > 10:
                logger.debug(f"Discarding navg>10 measurement to kill crazy TF: {m}")
                continue

            if m.coh >= 0.99999:
                logger.debug(f"Discarding coh=1 measurement: {m}")
                continue

            logger.info(f"Including HF TF {m}")
            mlist.append(m)

        # filter out any measurements in a different epoch
        for m in mlist:
            m_start, m_end = m.gps_segment

            # As per Cal meeting on 01/31/2024: we will allow the processing of
            # HFRL measurements from previous epochs if we don't anticipate any
            # significant changes. So I comment out the following three
            # lines. -LD

            # if not m_start >= epoch_start.gps():
            #     # print("Discarding measurement before "
            #     #       f"epoch start ({m_start}): {m}")
            #     continue

            # Don't use HFRL measurements that include data from after
            # the epoch ends. I.E. don't use measurements from the future.
            if not m_end <= epoch_end.gps():
                # print("Discarding measurement after "
                #       f"epoch end ({m_end}): {m}")
                continue

            # FIXME: fix this obscene hack used due to time constraints.
            if IFO == 'H1':
                boundary_60W = 1371417340
                if report.id_gpstime().gps() >= boundary_60W:
                    # only keep HFRL measurements that started after the
                    # 60W input power change.
                    if not m_start >= boundary_60W:
                        continue
                else:
                    # before the 60W only look at current epoch
                    if not m_start >= epoch_start.gps():
                        continue

            hf_measurements.append(m)
    logger.info("Including high frequency measurements from")
    logger.info(f"{epoch_start} to {epoch_end}")
    # write log of high freq roaming line calcs included
    with open(report.gen_path("hf_roaming_lines.txt"), 'w') as f:
        fhdr = ("# Frequency (Hz), tf mag, tf phase (rad), coherence, "
                "# of averages, "
                "Kappa_C, Fcc, gps start, gps end, datetime start, "
                "datetime end")
        f.write(fhdr + '\n')
        for hfm in hf_measurements:
            f.write(str(hfm) + "\n")

    hf_freqs = [x.freq for x in hf_measurements]

    hf_meas_x = list(map(lambda x: (ref_report.model_file, x),
                         hf_measurements))

    # run GPR
    frequencies = np.sort(list(frequencies) + hf_freqs)
    median, unc, cov, rsdls, tdcfs, gpr_ = measurement_list[ref_idx].run_gpr(
        frequencies, measurement_list,
        fmin_list[ref_idx], fmax_list[ref_idx],
        fmin_list=fmin_list, fmax_list=fmax_list,
        gpr_flim=(sensing_gpr_frange[0], sensing_gpr_frange[1]),
        save_to_file=report.gen_path('sensing_gpr.hdf5'),
        roaming_measurement_list_x=hf_meas_x,
        RBF_length_scale=rbf_length_scale,
        RBF_length_scale_limits=rbf_length_scale_limits
    )

    #  ============================ Plots ===============================
    mag = np.abs(median)
    phase = np.angle(median) * 180.0 / np.pi

    stacked_meas, tdcfs = measurement_list[ref_idx].stack_measurements(
        measurement_list,
        fmin_list[ref_idx], fmax_list[ref_idx], fmin_list, fmax_list,
        roaming_measurement_list_x=hf_meas_x
    )

    sensing_gpr_figs = []
    fig, axes = plt.subplots(nrows=2, ncols=1)
    sensing_gpr_figs.append(fig)
    fig.suptitle("Sensing GPR")
    ax0, ax1 = axes.flat

    ax0.plot(frequencies, mag, 'b-', label='Median')
    ax0.fill_between(frequencies, mag - unc, mag + unc, alpha=0.5,
                     fc='b', label='68% C.I.')
    ax0.set_xlim(frange_plot)
    ax0.set_ylim([0.80, 1.2])
    ax0.yaxis.set_minor_locator(ticker.MultipleLocator(0.02))
    ax0.set_ylabel(r'Mag (meas/model)')

    ax1.plot(frequencies, phase, 'b-', label='Median')
    ax1.fill_between(
        frequencies, phase - unc*180.0/np.pi, phase + unc*180.0/np.pi,
        alpha=0.5, fc='b', label='68% C.I.',
    )

    plot_handles = []
    plot_labels = []
    for i, meas in enumerate(stacked_meas):
        errbar_handle = ax0.errorbar(
            meas[0], np.abs(meas[4]),
            marker='o', markersize=10, linestyle='',
            yerr=meas[3],
        )

        # only add the following handles and labels for measurements
        # included as part of a sweep, not the high freq roaming line
        # measurements
        if i < len(timestamp_list):
            plot_handles.append(errbar_handle)
            plot_labels.append(f"meas. {timestamp_list[i]} of "
                               f"report {reports[i].id}")

        ax1.errorbar(
            meas[0],
            np.angle(meas[4])*180.0/np.pi,
            marker='o', markersize=10,
            linestyle='', yerr=meas[3]*180.0/np.pi,
        )

    ax1.set_xlim(frange_plot)
    ax1.set_ylim([-15, 15])
    ax1.yaxis.set_minor_locator(ticker.MultipleLocator(2))
    ax1.set_xlabel(r'Frequency (Hz)')
    ax1.set_ylabel(r'Phase (meas/model) [deg]')

    for ax in axes.flat:
        ax.grid(which='major', color='black')
        ax.grid(which='minor', ls='--')
        ax.set_xscale('log')
        ax.legend(loc='lower center', ncol=2, handlelength=2)
        ax.axvline(sensing_gpr_frange[0], ls='--', color='C06')
        ax.axvline(sensing_gpr_frange[1], ls='--', color='C06')

    fig.tight_layout(rect=(0, 0, 1, .87))
    # ax0.legend(
    fig.legend(
        handles=plot_handles,
        labels=plot_labels,
        bbox_to_anchor=(.05, .83, .92, .1),
        ncol=3,
        mode='expand',
        fontsize='small',
        numpoints=1,
        markerscale=1,
        bbox_transform=fig.transFigure,
        loc='lower left',
    )

    plt.savefig(report.gen_path('sensing_gpr.png'), bbox_inches='tight',
                pad_inches=0.2)
    return sensing_gpr_figs


def actuation_GPR(reports):
    reports = deepcopy(reports)
    report = reports[0]

    # use the last exported report in the current epoch as reference.
    # if it isn't available then use the earliest valid report
    if len(reports) > 1:
        ref_report = get_last_report_by_tag(reports, 'exported')
        ref_report_tag = 'last exported'
        if ref_report is None:
            ref_report = get_last_report_by_tag(reports[::-1], 'valid')
            ref_report_tag = 'first valid'
        if ref_report is None:
            ref_report = report
            ref_report_tag = 'this'
    else:
        ref_report = report
        ref_report_tag = 'this'
    logger.info(f"Using {ref_report_tag} report as Actuation GPR reference: "
                f"{ref_report.id}")

    base_config = ref_report.config['Actuation']

    actuation_gpr_figs = []
    frequencies = np.logspace(np.log10(1), np.log10(5000), 100)

    # FIXME: take values from config['Actuation'][<stage>] section
    UIM_gpr_frange = [10, 50]
    PUM_gpr_frange = [10, 500]
    TST_gpr_frange = [10, 800]

    stage_frange_dict = {
        'L1': UIM_gpr_frange,
        'L2': PUM_gpr_frange,
        'L3': TST_gpr_frange,
    }

    # keep a list of actuation measurements and their mcmc fit ranges by
    # stage and by optic.
    act_meas_dict = {
        'L1': {'EX': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []},
               'EY': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []}},
        'L2': {'EX': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []},
               'EY': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []}},
        'L3': {'EX': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []},
               'EY': {'fmin': [], 'fmax': [],
                      'measurements': [], 'timestamps': []}},
    }

    for mi, ireport in enumerate(reports):

        for stage, optics in base_config.items():
            if stage == 'params':
                continue

            for optic, config in optics.items():
                if optic == 'params':
                    continue

                name = f'Actuation/{stage}/{optic}'

                # here we grab the corresponding measurement from the
                # reference report.
                ref_processed_meas, ref_timestamp = \
                    ref_report.measurements[name]

                # we use act_val to avoid having to write the dictionary
                # indexing every time it needs to be accessed
                act_val = act_meas_dict[stage][optic]

                # we add the reference measurement if it isn't already in the
                # list. The reference should be first to be added to the list.
                if ref_processed_meas not in act_val['measurements']:
                    act_val['measurements'].append(ref_processed_meas)
                    act_val['timestamps'].append(ref_timestamp)
                    ref_mcmc_data = ref_report.get_act_mcmc_results()
                    ref_act_unit_mcmc_params = ref_mcmc_data['/'.join([stage,
                                                                       optic])]
                    act_val['fmin'].append(ref_act_unit_mcmc_params['fmin'])
                    act_val['fmax'].append(ref_act_unit_mcmc_params['fmax'])

                # now we take the corresponding measurement from the
                # report we're iterating over
                processed_actuation, timestamp = ireport.measurements[name]
                # skip duplicate measurements
                # Now we determine whether to add the "current" report's
                # measurement to the list.
                if timestamp in act_val['timestamps']:
                    continue

                mcmc_data = ireport.get_act_mcmc_results()
                act_unit_mcmc_params = mcmc_data['/'.join([stage, optic])]

                act_val['timestamps'].append(timestamp)
                act_val['measurements'].append(processed_actuation)
                act_val['fmin'].append(act_unit_mcmc_params['fmin'])
                act_val['fmax'].append(act_unit_mcmc_params['fmax'])

    # run GPR for all measurements
    for stage, optics in base_config.items():
        if stage == 'params':
            continue
        for optic, config in optics.items():
            if optic == 'params':
                continue

            meas_list = act_meas_dict[stage][optic]['measurements']
            fmin_list = act_meas_dict[stage][optic]['fmin']
            fmax_list = act_meas_dict[stage][optic]['fmax']
            ref_meas = ref_report.measurements[f'Actuation/{stage}/{optic}'][0]
            ref_meas_idx = meas_list.index(ref_meas)

            gpr_fname = f'actuation_{stage}_{optic}_gpr.hdf5'
            median, unc, cov, residuals, tdcfs = ref_meas.run_gpr(
                frequencies, deepcopy(meas_list),
                fmin_list[ref_meas_idx], fmax_list[ref_meas_idx],
                fmin_list=fmin_list, fmax_list=fmax_list,
                gpr_flim=(stage_frange_dict[stage][0],
                          stage_frange_dict[stage][1]),
                save_to_file=report.gen_path(gpr_fname),
            )

            #  ============================ Plots =============================
            frange_plot = [5, 2000]  # FIXME: make tunable via cmd config
            mag = np.abs(median)
            phase = np.angle(median)*180.0 / np.pi

            stacked_meas, tdcfs = ref_meas.stack_measurements(
                meas_list,
                fmin_list[ref_meas_idx],
                fmax_list[ref_meas_idx],
                fmin_list,
                fmax_list,
            )

            name = f'Actuation/{stage}/{optic}'
            fig, axes = plt.subplots(nrows=2, ncols=1)
            fig.suptitle(f"{name} GPR")
            ax0, ax1 = axes.flat

            ax0.plot(frequencies, mag, 'b-', label='Median')
            ax0.fill_between(frequencies, mag - unc, mag + unc,
                             alpha=0.5, fc='b', label='68% C.I.')

            plot_handles = []
            plot_labels = []
            for i, meas in enumerate(stacked_meas):
                errbar_handle = ax0.errorbar(
                    meas[0],
                    np.abs(meas[4]),
                    marker='o',
                    markersize=10, linestyle='',
                    yerr=meas[3],
                )
                plot_handles.append(errbar_handle)
                plot_labels.append(f"{reports[i].id}")

                ax1.errorbar(meas[0],
                             np.angle(meas[4])*180.0/np.pi,
                             marker='o', markersize=10, linestyle='',
                             yerr=meas[3]*180.0/np.pi)

            ax0.set_xlim(frange_plot)
            ax0.set_ylim([0.9, 1.1])
            ax0.yaxis.set_minor_locator(ticker.MultipleLocator(0.01))
            ax0.set_ylabel(r'Mag (meas/model)')

            ax1.plot(frequencies, phase, 'b-', label='Median')
            ax1.fill_between(frequencies, phase - unc*180.0/np.pi,
                             phase + unc*180.0/np.pi,
                             alpha=0.5, fc='b', label='68% C.I.')
            ax1.set_xlim(frange_plot)
            ax1.set_ylim([-5, 5])
            ax1.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
            ax1.set_xlabel(r'Frequency (Hz)')
            ax1.set_ylabel(r'Phase (meas/model) [deg]')

            for ax in axes.flat:
                ax.grid(which='major', color='black')
                ax.grid(which='minor', ls='--')
                ax.set_xscale('log')
                ax.legend(loc='lower center', ncol=2, handlelength=2)
                ax.axvline(stage_frange_dict[stage][0], ls='--', color='C06')
                ax.axvline(stage_frange_dict[stage][1], ls='--', color='C06')

            ax0.legend(
                handles=plot_handles,
                labels=plot_labels,
                loc='lower left',
            )

            plt.savefig(
                report.gen_path(f'actuation_{stage}_{optic}_gpr.png'),
                bbox_inches='tight',
                pad_inches=0.2,
            )
            actuation_gpr_figs.append(fig)

    return actuation_gpr_figs


def sync_hfrl_measurements(config, outpath):
    """Sync high frequency roaming line measurements

    """
    # FIXME: replace with arx call or with https call that doesn't require auth
    # this path construction ensures a closing slash on the path, so
    # we make sure only the directory contents are synced
    hfrl_location = os.path.join(config['Common']['hfrl_location'], '')
    sync_cmd = ['rsync', '-avh', '--progress', hfrl_location, outpath]
    try:
        run(sync_cmd, check=True)
    except Exception:
        logger.warning("WARNING: Unable to sync HFRL measurements.")


def broadband_plot(report):
    new_report = deepcopy(report)

    freqs, processed_bb, timestamp = new_report.measurements['Broadband']
    syserr_fig = plt.figure()
    syserr_fig.suptitle(f"Pcal/DeltaL_External {timestamp}")
    ax_mag, ax_pha = syserr_fig.subplots(2, 1, sharex=True)
    ax_mag.semilogx(freqs, np.abs(processed_bb))
    ax_pha.semilogx(freqs, np.angle(processed_bb, deg=True))

    ax_mag.set_ylim(.85, 1.15)
    ax_mag.set_xlim(7, 500)
    ax_mag.grid(which='major', color='black')
    ax_mag.grid(which='minor', ls='--')
    ax_mag.set_ylabel('Magnitude')
    ax_mag.yaxis.set_minor_locator(ticker.MultipleLocator(0.01))

    ax_pha.set_ylim(-10, 10)
    ax_pha.yaxis.set_minor_locator(ticker.MultipleLocator(1))
    ax_pha.grid(which='major', color='black')
    ax_pha.grid(which='minor', ls='--')
    ax_pha.set_xlabel('Frequency [Hz]')
    ax_pha.set_ylabel('Phase [deg]')

    return [syserr_fig]


def gds_filter_generation(report):
    report = deepcopy(report)
    filters_fname = f'gstlal_compute_strain_C00_filters_{IFO}.npz'
    fir_filter_gen = FIRFilterFileGeneration(report.model_file)
    fir_filter_gen.GDS(output_dir=report.path,
                       output_filename=filters_fname,
                       plots_directory=report.gen_path('fir_plots')
                       )
    figs = fir_filter_gen.figs
    return figs


def write_pydarm_version(report):
    """record the pydarm version to a file"""
    with open(os.path.join(report.path, 'pydarm_version'), 'w') as f:
        f.write(f"{__version__}\n")


class ReportPDF:

    def __init__(self, path):
        self.path = path

    def __enter__(self):
        self.pdf = PdfPages(self.path)
        return self

    def add_figure(self, new_fig):
        if hasattr(new_fig, '__iter__'):
            for f in new_fig:
                self.pdf.savefig(f)
        else:
            self.pdf.savefig(new_fig)

    def add_figures(self, new_figs):
        for fig in new_figs:
            self.add_figure(fig)

    def __exit__(self, exception_type, exception_value, traceback):
        self.pdf.close()
        if exception_type is not None:
            os.unlink(self.path)
        return False


def add_args(parser):
    parser.add_argument(
        "time", metavar='DATETIME/REPORT_ID', nargs='?', default='now',
        help="specify date/time or report ID (default: 'now')")
    _args.add_model_option(parser)
    _args.add_config_option(parser)
    parser.add_argument(
        '--outdir', '-o', metavar='DIR',
        help="write report to specified directory")
    parser.add_argument(
        '--force', action='store_true',
        help="force generate report if it already exists")

    mgroup = parser.add_mutually_exclusive_group()
    mgroup.add_argument(
        '--mcmc-only', action='store_const', dest='mcmc', const='only',
        help="run MCMC only then exit")
    mgroup.add_argument(
        '--skip-mcmc', action='store_const', dest='mcmc', const='skip',
        help="skip MCMC")

    egroup = parser.add_mutually_exclusive_group()
    egroup.add_argument(
        '--epoch-sensing', dest='epoch', action='store_const',
        const=set(['sensing']),
        help="mark this report as the start of a new sensing epoch")
    egroup.add_argument(
        '--epoch-actuation', dest='epoch', action='store_const',
        const=set(['actuation']),
        help="mark this report as the start of a new actuation epoch")
    egroup.add_argument(
        '--epoch', dest='epoch', action='store_const',
        const=set(['sensing', 'actuation']),
        help=("mark this report as the start of a new epoch for "
              "both sensing and actuation"))
    parser.set_defaults(epoch=set([]))

    parser.add_argument(
        '--gds-filters', action=argparse.BooleanOptionalAction,
        default=True,
        help="generate GDS FIR filters")
    parser.add_argument(
        '-i', '--interactive', action='store_true',
        help="interactive matplotlib plots")
    parser.add_argument(
        '--display', action=argparse.BooleanOptionalAction, default=True,
        help="display report after generation or not")
    parser.add_argument(
        '--no-bb', action='store_true',
        help="skip PCAL2DARM bb measurement"
    )


def main(args):
    """generate/view full calibration report

    Calibration reports are based on measurement sets, which are
    complete sets of all the sensing, actuation, and PCal response
    function measurements needed to model the calibration function.
    The measurements themselves are executed with the `measure`
    sub-command.

    A measurement set for a given time consists of the most recent
    sensing/actuaion/PCal measurements before that time.  Each
    measurement set is identified by the date-time of its latest
    measurement.  For any measurement set there can be a report whose
    ID is the timestamp of the corresponding measurement set.  The
    command is able to gather measurement sets from the available
    sensing/actuation/pcal measurements, and generate reports based on
    them.

    Calibration reports consist of the following for each measurement
    type: MCMC summary (including a corner plot), a "history" plot,
    and a GPR result. In addition, fresh FIR filters are generated along
    with the generation of each new report based on the pyDARM parameter
    model set once it's been populated with the free parameters as estimated
    by the MCMC process.

    A report tagging system is used to indicate the set of reports to consider
    in the "history" plots and the GPR processing, as well as to identify
    which report's model should be considered the set's "reference".

    We define an `epoch` to be the first report in a given set (i.e. the
    report that marks the starting boundary of a set). The end of each set
    is marked by the start of the next epoch or, if there is none, the most
    recent report. The `epoch` tag is interpreted as representing the start of
    an epoch for the sensing and acutation functions simultaneously. Similarly,
    the `epoch-sensing` and `epoch-actuation` tags do the same for only their
    corresponding measurements.

    The `valid` tag identifies reports that should be included in the GPR
    processing and the "history" comparison plots.

    The `exported` tag is created when a report has been exported to the
    front end at either L1 or H1. The most recent exported report should
    be considered the reference (i.e. containing the reference model) in
    the MCMC residuals, GPR, and history residuals.

    If no arguments are provided, this command will generate or view
    the report corresponding to the latest measurement set.  If a time
    argument is provided, the latest measurement set for that time
    will be processed.  If a report ID is provided, the report
    corresponding to that ID will be produced.
    """
    logger.info(f"config file: {args.config}")
    logger.info(f"model file: {args.model}")
    plt.interactive(args.interactive)

    try:
        report = Report.find(args.time)
        mset = report.mset
    except FileNotFoundError:
        logger.info("searching measurement sets...")
        mset = MeasurementSet.find(args.time, config_file=args.config,
                                   skip_bb=args.no_bb)
        logger.info(f"measurement set found: {mset.id}")
        try:
            report = Report.find(mset.id)
            logger.info(f"existing report found: {report.path}")
        except FileNotFoundError:
            report = None

    if args.outdir:
        report_dir = args.outdir
        try:
            report = Report(report_dir)
            logger.info(f"report found at alternate report path: {report_dir}")
        except FileNotFoundError:
            report = None
            logger.info(f"using alternate report path: {report_dir}")
    elif report:
        report_dir = report.path
    else:
        report_dir = Report.default_path(mset.id)

    if report:
        if args.force:
            logger.info("force regenerating existing report...")
            report = None
    else:
        logger.info(f"generating report {report_dir}...")

    if report is None:
        # mcmc is skipped then use the populated model file at the destination
        # since we're assuming that the report directory contains a
        # model file with populated free params and inverse sensing
        # function foton tf
        if args.mcmc == 'skip':
            logger.info("skipping MCMC generation.")
            logger.warning(f"Assuming that a populated pydarm_{IFO}.ini"
                           " exists in the report directory.")
            model_file = os.path.join(report_dir, f'pydarm_{IFO}.ini')
            report = Report.create(
                mset,
                args.config,
                model_file,
                path=report_dir,
            )
            write_pydarm_version(report)
            report.load_measurements()
            logger.info(f"using pre-populated model file: {model_file}")
        else:
            report = Report.create(
                mset,
                args.config,
                args.model,
                path=report_dir,
            )
            write_pydarm_version(report)
            report.load_measurements()
            gen_sensing_MCMC(report)
            gen_actuation_MCMC(report)
            logger.info("MCMC generation complete.")

            # update model ini with new fit params
            model_config = report.model._config
            params = report.gen_free_params_ini_dict()

            logger.info("writing fit parameters to model file "
                        f"{report.model_file}...")
            base, ext = os.path.splitext(report.model_file)
            orig_out = base + '.orig' + ext
            orig_parsed_out = base + '.orig-parsed' + ext
            os.rename(report.model_file, orig_out)
            with open(orig_parsed_out, 'w') as f:
                model_config.write(f)
            model_config.read_dict(params)
            with open(report.model_file, 'w') as f:
                model_config.write(f)

            if args.mcmc == 'only':
                return

        sync_hfrl_measurements(
            report.config,
            report.gen_mpath('hf_roaming_lines'),
        )

        ##########
        # sensing

        # find past reports in sensing epoch
        past_reports_sensing = []
        if 'sensing' in args.epoch:
            logger.info("new epoch, ignoring past reports for sensing.")
            report._add_tags('epoch-sensing')
        else:
            logger.info("scanning past reports for sensing epoch...")
            for pr in list_all_reports():
                if pr.id == report.id:
                    continue
                if pr.id_gpstime() >= report.id_gpstime():
                    continue
                if 'valid' in pr.tags:
                    logger.info(f"  {pr.id}")
                    # FIXME: THIS NEEDS TO USE CURRENT MODEL
                    # NOT OLD REPORT MODEL
                    pr.load_measurements()
                    past_reports_sensing.append(pr)
                else:
                    logger.info(f"Skipping report missing valid tag: {pr.id}")

                # FIXME: should we check for diffs in the model ini,
                # since it's changed in model parameters that's what
                # really defines the epoch boundaries?
                if set(['epoch-sensing', 'epoch']) & set(pr.tags):
                    logger.info(f"sensing epoch boundary at {pr.id}")
                    break

        logger.info("generating sensing history...")
        fig_sens_hist = sensing_history(report, past_reports_sensing)
        logger.info("generating sensing GPR...")
        fig_sens_gpr = sensing_GPR([report] + past_reports_sensing)

        ##########
        # actuation

        # find past reports in actuation epoch
        past_reports_actuation = []
        if 'actuation' in args.epoch:
            logger.info("new epoch, ignoring past reports for actuation.")
            report._add_tags('epoch-actuation')
        else:
            logger.info("scanning past reports for actuation epoch...")
            for pr in list_all_reports():
                if pr.id == report.id:
                    continue
                if pr.id_gpstime() >= report.id_gpstime():
                    continue
                if 'valid' in pr.tags:
                    logger.info(f"  {pr.id}")
                    # FIXME: THIS NEEDS TO USE CURRENT MODEL
                    # NOT OLD REPORT MODEL
                    pr.load_measurements()
                    past_reports_actuation.append(pr)
                else:
                    logger.info(f"Skipping report missing valid tag: {pr.id}")

                # FIXME: should we check for diffs in the model ini,
                # since it's changed in model parameters that's what
                # really defines the epoch boundaries?
                if set(['epoch-actuation', 'epoch']) & set(pr.tags):
                    logger.info(f"actuation epoch boundary at {pr.id}")
                    break

        logger.info("generating actuation history...")
        fig_act_hist = actuation_history(report, past_reports_actuation)
        logger.info("generating actuation GPR...")
        fig_act_gpr = actuation_GPR([report] + past_reports_actuation)

        # Export coupled cavity pole frequency and 7kHz pole for
        # inverse sensing path according to LHO:47948
        rsensing = report.model.sensing
        sensing_sign = rsensing.sensing_sign
        if IFO == 'H1':
            zpk_params = ([rsensing.coupled_cavity_pole_frequency],
                          [7000], sensing_sign)
        elif IFO == 'L1':
            chain = read_chain_from_hdf5(os.path.join(report.path,
                                                      'sensing_mcmc_chain.hdf5'
                                                      ))
            ch = chain[-1]
            proc_sensing_meas = report.measurements['Sensing'][0]
            z_str, p_str, _, g_str = \
                proc_sensing_meas.inverse_sensing_srcd2n_foton_filter_zpk(ch)
            chain = ch
            sign_char = '-' if str(sensing_sign).startswith('-') else ''
            zeros = np.array(z_str.strip('[]').split(';'), dtype=float)
            poles = np.array(p_str.strip('[]').split(';'), dtype=float)
            gain_val = float(sign_char + g_str)

            zpk_params = (zeros, poles, gain_val)

        #######################################################
        # write and place extra files in the report directory #
        #######################################################

        # generate inverse sensing foton tf and place it in the cal svn
        tf_dir = os.path.join(os.environ['CAL_DATA_ROOT'],
                              "Runs/O4", IFO, "Measurements/Foton")
        tf_fname = f"{report.id}_{IFO}CALCS_InverseSensingFunction_Foton_tf.txt"
        inv_sensing_tf_file = os.path.abspath(os.path.join(tf_dir, tf_fname))

        os.makedirs(tf_dir, exist_ok=True)
        write_foton_tf_to_file(zpk_params, 0.01, 10000, 1001,
                               inv_sensing_tf_file)

        model_config = report.model._config
        model_config.read_dict({'calcs': {'foton_invsensing_tf':
                                          inv_sensing_tf_file}})
        with open(report.model_file, 'w') as f:
            model_config.write(f)

        write_dtt_deltal_external_calib(report)
        write_dtt_pcal_calib(report)
        write_epics_records_file(report)

        # GDS FIR filter generation
        fig_gds_filt = None
        if args.gds_filters:
            logger.info("generating GDS filters...")
            fig_gds_filt = gds_filter_generation(report)

        ##########
        # write report PDF

        with ReportPDF(report.report_file) as rpdf:
            rpdf.add_figures(fig_sens_hist)
            rpdf.add_figures(fig_sens_gpr)
            rpdf.add_figures(fig_act_hist)
            rpdf.add_figures(fig_act_gpr)
            if fig_gds_filt:
                rpdf.add_figures(fig_gds_filt)

        logger.info("report generation complete.")
        logger.info(f"report file: {report.report_file}")
        if args.interactive:
            input('press enter to exit')

    if args.display:
        if not os.path.exists(report.report_file):
            raise CMDError(f"report file not found! ({report.report_file})")
        logger.info(f"displaying report {report.report_file}...")
        subprocess.run(['xdg-open', report.report_file], check=True)
